def makeBoard():
    list1=["(  ) ","(  ) ","(  ) ","(  ) ","(  ) ","(  ) ","(  ) "]
    list2=["(  ) ","(  ) ","(  ) ","(  ) ","(  ) ","(  ) ","(  ) "]
    list3=["(  ) ","(  ) ","(  ) ","(  ) ","(  ) ","(  ) ","(  ) "]
    list4=["(  ) ","(  ) ","(  ) ","(  ) ","(  ) ","(  ) ","(  ) "]
    list5=["(  ) ","(  ) ","(  ) ","(  ) ","(  ) ","(  ) ","(  ) "]
    list6=["(  ) ","(  ) ","(  ) ","(  ) ","(  ) ","(  ) ","(  ) "]
    list0 = [list1 , list2 , list3 , list4 , list5 , list6]
    return list0

def printBoard(board):
    for a in range(6):
        print()
        for b in range(7):
            print(board[a][b], end="")  
    print()

#piece is a string
#allows user to take turn
def playerTurn(piece, board): 
    c=int(input("which column do you want to place your chip in?"))
    r=len(board)-1
    while(r >= 0 and board[r][c] != "(  ) "):
        r=r-1
    if r < 0: #this means there are no open spots in row
        print("Hey there is no room there")
        playerTurn(piece, board)
    else: #we have the index for where the piece should go
        board[r][c]= piece 
    
def playerCheck(board, piece):
    #checks horizontal
    #e is the row
    #f is the column
    for e in range(len(board)):
        for f in range(len(board[0])-3):
            if list0[e][f]==piece and list0[e][f+1]==piece and list0[e][f+2]==piece and list0[e][f+3]==piece:
                print(piece + "you won!")
                return True
            
        
  
  
    #checks vertical
    #k is the row
    #m is the column
    for k in range(len(board)-3):
        for m in range(len(board[0])):
            if list0[k][m]==piece and list0[k+1][m]==piece and list0[k+2][m]==piece and list0[k+3][m]==piece:
                print(piece + "you won!")
                return True
    
    #checks diagonal(bottom is on the left top is on the right)
    j=0                    
    while j<3:
        x=0    
        while x<4:
            if list0[j][x]==piece and list[j+1][x+1]==piece and list[j+2][x+2]==piece and list[j+3][x+3]==piece:
                print(piece + "you won!")
                return True
            x=x+1
        j=j+1
    #checks diagonal(bottom is on the right top is on the left)  
    u=0
    while u<3:
        i=3
        while i<7:
            if list0[u][i]==piece and list0[u+1][i-1]==piece and list0[u+2][i-2]==piece and list0[u+3][i-3]==piece:
                print(piece + "you won!")
                return True
            i=i+1
        u=u+1
    return False
        
while True:
    list7=[5, 5, 5, 5, 5, 5, 5]
    
    list0=makeBoard()
    printBoard(list0)
    for d in range(42):
        piece="(😡) "
        playerTurn(piece, list0)
        printBoard(list0)
        if playerCheck(list0, piece):
            break
        piece = "(🙂) "
        playerTurn(piece, list0)
        printBoard(list0)
        if playerCheck(list0, piece):
            break
    z=input("would you like to play again?")
    
    if z!="yes":
        break
