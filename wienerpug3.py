'''
Created on Oct 18, 2018

@author: coditum
'''

num=int(input("select a number from 3-10 for your triangle size."))


for a in range(num):
    
    
    for space in range(num-a-1): 
        print(" ", end="")   
    
    for s in range(a+1):
        print("*", end=" ")
    print() 
    
      